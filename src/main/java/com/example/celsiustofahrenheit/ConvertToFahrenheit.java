package com.example.celsiustofahrenheit;

public class ConvertToFahrenheit {
    public double convert(double temperature) {
        return (9.0 / 5.0) * temperature + 32;
    }
}
