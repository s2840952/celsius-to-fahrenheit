package com.example.celsiustofahrenheit;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private ConvertToFahrenheit temperature;

    public void init() {
        temperature = new ConvertToFahrenheit();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>Temperature in Celsius: " + request.getParameter("temperature") + "</h1>");
        out.println("<h1>Temperature in Fahrenheit: " + temperature.convert(Double.parseDouble(request.getParameter("temperature"))) + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}